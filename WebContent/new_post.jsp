<%@page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>新規投稿</title>
<link href="./css/style.css" rel="stylesheet" type="text/css">
</head>
<body>
	<c:if test="${ not empty errorMessages }">
			<div class="errorMessages">
				<ul>
					<c:forEach items="${errorMessages}" var="message">
						<li><c:out value="${message}" />
					</c:forEach>
				</ul>
			</div>
			<c:remove var="errorMessages" scope="session" />
		</c:if>
	<div class="form-area">
		<form action="newMessage" method="post">
			<label for="subject">件名</label> <input name="subject" id="subject" />
			<br /> <label for="text">本文</label>
			<textarea name="text" cols="100" rows="5" id="text"></textarea>
			<br /> <label for="category">カテゴリー</label> <input name="category"
				id="category" /> <br /> <input type="hidden" name="deleteId"
				value="0"> <br /> <input type="submit" value="投稿">
			<a href="top">戻る</a>
		</form>
	</div>
	<div class="copylight">Copyright(c)YourName</div>
</body>
</html>