<%@page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>掲示板システム</title>
<link href="./css/style.css" rel="stylesheet" type="text/css">
</head>
<body>
	<div class="main-contents">
		<div class="header">
			<c:if test="${ empty loginUser }">
				<a href="./">ログイン</a>

			</c:if>
			<c:if test="${ not empty loginUser }">
				<a href="newMessage">新規投稿</a>

				<a href="userManagement">ユーザー管理</a>

				<a href="logout">ログアウト</a>
			</c:if>
		</div>

		<c:if test="${ not empty errorMessages }">
			<div class="errorMessages">
				<ul>
					<c:forEach items="${errorMessages}" var="message">
						<li><c:out value="${message}" />
					</c:forEach>
				</ul>
			</div>
			<c:remove var="errorMessages" scope="session" />
		</c:if>

		<p>カテゴリ検索</P>
		<form action="top" method="get">
		<input type="text" name="keyword">
		<input type="submit" value="検索">
		</form>

		<p>投稿日検索</P>
		<form action="top" method="get">
		<label>期間 <input type="date" name="startDate"></label>
		<label>～<input type="date" name="lastDate"></label>
		<input type="submit" value="検索">
		</form>

		<div class="messages">
			<c:forEach items="${messages}" var="message">
				<c:if test="${message.deleteId == 0}">
					<div class="message">
						件名:<br />
						<div class="subject">
							<c:out value="${message.subject}" />
						</div>
						本文:<br />
						<div class="text">
							<c:out value="${message.text}" />
						</div>
						カテゴリ:<br />
						<div class="category">
							<c:out value="${message.category}" />
						</div>
						投稿日:<br />
						<div class="date">
							<fmt:formatDate value="${message.created_date}"
								pattern="yyyy/MM/dd HH:mm:ss" />
						</div>
						投稿者:<br />
						<div class="name">
							<c:out value="${message.name}" />
						</div>
						<c:if test="${message.userId == loginUser.id }">
							<form action="messageDelete" method="post">
								<input type="hidden" name="deleteId" value="1"> <input
									type="hidden" name="id" value="${message.id }"> <input
									type="submit" value="投稿削除">
							</form>
						</c:if>
					</div>

					<div class="comments">
						<c:forEach items="${comments}" var="comment">
							<c:if test="${message.id == comment.postId }">
								<c:if test="${comment.deleteId == 0}">
									<div class="comment">
										コメント:<br />
										<div class="post">
											<c:out value="${comment.post}" />
										</div>
										コメント投稿日:<br />
										<div class="date">
											<fmt:formatDate value="${comment.created_date}"
												pattern="yyyy/MM/dd HH:mm:ss" />
										</div>
										コメント投稿者:<br />
										<div class="name">
											<c:out value="${comment.name}" />
										</div>
										<c:if test="${comment.userId == loginUser.id }">
											<form action="commentDelete" method="post">
												<input type="hidden" name="deleteId" value="1"> <input
													type="hidden" name="id" value="${comment.id }"> <input
													type="submit" value="コメント削除">
											</form>
										</c:if>
									</div>
								</c:if>
							</c:if>
						</c:forEach>
					</div>
					<div class="form-area">
                        <c:if test="${ isShowMessageForm }">
							<form action="comment" method="post">
								<textarea name="comment" cols="100" rows="5" class="tweet-box"></textarea>
								<br /> <input type="hidden" name="deleteId" value="0">
								<input type="hidden" name="postId" value="${message.id }">
								<input type="submit" value="コメント">（500文字まで）
							</form>
                        </c:if>
					</div>
				</c:if>
			</c:forEach>
		</div>
		<div class="copylight">Copyright(c)YourName</div>
	</div>
</body>
</html>