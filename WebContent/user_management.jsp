<%@page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>ユーザー管理</title>
<link href="./css/style.css" rel="stylesheet" type="text/css">
</head>
<body>
	<a href="signup">ユーザー新規登録</a>
	<br />

	<caption>ユーザー登録情報一覧</caption>
	<table border=1>
		<tr>
			<th>名称</th>
			<th>支店</th>
			<th>部署・役職</th>
			<th>ログインID</th>
			<th>編集</th>
			<th>ユーザー管理</th>
		</tr>
		<c:forEach items="${users}" var="user">
			<tr>
				<td><div class="name">
						<c:out value="${user.name}" />
					</div></td>
				<td><div class="branchName">
						<c:out value="${user.branchName}" />
					</div></td>
				<td><div class="departmentName">
						<c:out value="${user.departmentName}" />
					</div></td>
				<td><div class="login_id">
						<c:out value="${user.login_id}" />
					</div></td>
				<td><a href="settings?id=${user.id}">編集</a></td>
				<c:if test="${user.stopId == 0}">
					<td>
						<form action="userManagement" method="post">
							<input type="hidden" name="stopId" value="1"> <input
								type="hidden" name="id" value="${user.id }"> <input
								type="submit" value="停止" onclick='return confirm("本当に停止しますか");' />
						</form>
					</td>
				</c:if>
				<c:if test="${user.stopId == 1}">
					<td>
						<form action="userManagement" method="post">
							<input type="hidden" name="stopId" value="0"> <input
								type="hidden" name="id" value="${user.id }"> <input
								type="submit" value="復活" onclick='return confirm("本当に復活しますか");' />
						</form>
					</td>
				</c:if>
			</tr>
		</c:forEach>
	</table>
	<a href="top">戻る</a>
	<div class="copylight">Copyright(c)YourName</div>
</body>
</html>