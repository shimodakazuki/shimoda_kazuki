<%@page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>ユーザー登録</title>
<link href="./css/style.css" rel="stylesheet" type="text/css">
</head>
<body>
	<div class="main-contents">
		<c:if test="${ not empty errorMessages }">
			<div class="errorMessages">
				<ul>
					<c:forEach items="${errorMessages}" var="message">
						<li><c:out value="${message}" />
					</c:forEach>
				</ul>
			</div>
			<c:remove var="errorMessages" scope="session" />
		</c:if>
		<form action="signup" method="post">
			<br />
			<label for="name">名称</label>
			<input name="name" id="name" />
			<br />
			<label for="login_id">ログインID</label>
			<input name="login_id" id="login_id" />
			<br />
			<label for="password">パスワード</label>
			<input name="password" type="password" id="password" />
			<br />
			<label for="confirmationPassword">パスワード(確認用)</label>
			<input name="confirmationPassword" type="Password" id="confirmationPassword" />
			<br />
			支店<br>
			<select name="branchId">
			<option value="1">本社</option>
			<option value="2">支店A</option>
			<option value="3">支店B</option>
			<option value="4">支店C</option>
			</select>
			<br>
			<br />
			部署・役職<br>
			<select name="departmentId">
			<option value="1">総務人事担当者</option>
			<option value="2">情報管理担当者</option>
			<option value="3">店長</option>
			<option value="4">社員</option>
			</select>
			<br>
			<br />
			<input type="hidden" name="stopId" value="0">
			<br />
			<input type="submit" value="登録" /> <br /> <a href="top">戻る</a>
		</form>
		<div class="copyright">Copyright(c)Your Name</div>
	</div>
</body>
</html>