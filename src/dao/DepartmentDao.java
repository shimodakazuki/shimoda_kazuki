package dao;

import static utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import beans.Department;
import exception.SQLRuntimeException;

public class DepartmentDao {

    public void insert(Connection connection, Department department) {

        PreparedStatement ps = null;
        try {
            StringBuilder sql = new StringBuilder();
            sql.append("INSERT INTO department ( ");
            sql.append("departmentName");
            sql.append(") VALUES (");
            sql.append(" ?"); //departmentName
            sql.append(")");

            ps = connection.prepareStatement(sql.toString());

            ps.setString(1, department.getDepartmentName());

            ps.executeUpdate();
        } catch (SQLException e) {
            throw new SQLRuntimeException(e);
        } finally {
            close(ps);
        }
    }

}