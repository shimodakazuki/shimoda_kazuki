package dao;

import static utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import beans.UserMessage;
import exception.SQLRuntimeException;

public class UserMessageDao {

    public List<UserMessage> getUserMessages(Connection connection, int num) {

        PreparedStatement ps = null;
        try {
            StringBuilder sql = new StringBuilder();
            sql.append("SELECT ");
            sql.append("messages.id as id, ");
            sql.append("messages.subject as subject, ");
            sql.append("messages.text as text, ");
            sql.append("messages.user_id as user_id, ");
            sql.append("messages.category as category, ");
            sql.append("messages.deleteId as deleteId, ");
            sql.append("users.name as name, ");
            sql.append("messages.created_date as created_date ");
            sql.append("FROM messages ");
            sql.append("INNER JOIN users ");
            sql.append("ON messages.user_id = users.id ");
            sql.append("ORDER BY created_date DESC limit " + num);

            ps = connection.prepareStatement(sql.toString());

            ResultSet rs = ps.executeQuery();
            List<UserMessage> ret = toUserMessageList(rs);
            return ret;
        } catch (SQLException e) {
            throw new SQLRuntimeException(e);
        } finally {
            close(ps);
        }
    }

    private List<UserMessage> toUserMessageList(ResultSet rs)
            throws SQLException {

        List<UserMessage> ret = new ArrayList<UserMessage>();
        try {
            while (rs.next()) {
                String subject = rs.getString("subject");
                String text = rs.getString("text");
                int id = rs.getInt("id");
                int userId = rs.getInt("user_id");
                String category = rs.getString("category");
                int deleteId = rs.getInt("deleteId");
                Timestamp createdDate = rs.getTimestamp("created_date");
                String name = rs.getString("name");

                UserMessage message = new UserMessage();
                message.setSubject(subject);
                message.setText(text);
                message.setId(id);
                message.setUserId(userId);
                message.setCategory(category);
                message.setDeleteId(deleteId);
                message.setCreated_date(createdDate);
                message.setName(name);

                ret.add(message);
            }
            return ret;
        } finally {
            close(rs);
        }
    }
    public List<UserMessage> getKeyword(Connection connection, String keyword, int num) {

        PreparedStatement ps = null;
        try {
            StringBuilder sql = new StringBuilder();
            sql.append("SELECT ");
            sql.append("messages.id as id, ");
            sql.append("messages.subject as subject, ");
            sql.append("messages.text as text, ");
            sql.append("messages.user_id as user_id, ");
            sql.append("messages.category as category, ");
            sql.append("messages.deleteId as deleteId, ");
            sql.append("users.name as name, ");
            sql.append("messages.created_date as created_date ");
            sql.append("FROM messages ");
            sql.append("INNER JOIN users ");
            sql.append("ON messages.user_id = users.id ");
            sql.append("WHERE ");
            sql.append("category ");
            sql.append("LIKE ? ");
            sql.append("ORDER BY created_date DESC limit " + num);

            ps = connection.prepareStatement(sql.toString());

            ps.setString(1, "%" + keyword + "%");

            ResultSet rs = ps.executeQuery();
            List<UserMessage> ret = toUserMessageList(rs);
            return ret;
        } catch (SQLException e) {
            throw new SQLRuntimeException(e);
        } finally {
            close(ps);
        }
    }
    public List<UserMessage> getDate(Connection connection, String startDate, String lastDate, int num) {

        PreparedStatement ps = null;
        try {
            StringBuilder sql = new StringBuilder();
            sql.append("SELECT ");
            sql.append("messages.id as id, ");
            sql.append("messages.subject as subject, ");
            sql.append("messages.text as text, ");
            sql.append("messages.user_id as user_id, ");
            sql.append("messages.category as category, ");
            sql.append("messages.deleteId as deleteId, ");
            sql.append("users.name as name, ");
            sql.append("messages.created_date as created_date ");
            sql.append("FROM messages ");
            sql.append("INNER JOIN users ");
            sql.append("ON messages.user_id = users.id ");
            sql.append("WHERE ");
            sql.append("messages.created_date ");
            sql.append(">= ? ");
            sql.append("AND ");
            sql.append("messages.created_date ");
            sql.append("< ? ");
            sql.append("ORDER BY created_date DESC limit " + num);

            ps = connection.prepareStatement(sql.toString());

            ps.setString(1, startDate + " 00:00:00");
            ps.setString(2, lastDate + " 23:59:59");

            ResultSet rs = ps.executeQuery();
            List<UserMessage> ret = toUserMessageList(rs);
            return ret;
        } catch (SQLException e) {
            throw new SQLRuntimeException(e);
        } finally {
            close(ps);
        }
    }
}