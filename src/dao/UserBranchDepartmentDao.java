package dao;

import static utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import beans.UserBranchDepartment;
import exception.SQLRuntimeException;

public class UserBranchDepartmentDao {

    public List<UserBranchDepartment> getUserBranchDepartments(Connection connection, int num) {

        PreparedStatement ps = null;
        try {
            StringBuilder sql = new StringBuilder();
            sql.append("SELECT ");
            sql.append("users.id as id, ");
            sql.append("users.stopId as stopId, ");
            sql.append("users.login_id as login_id, ");
            sql.append("users.branchId as branchId, ");
            sql.append("users.departmentId as departmentId, ");
            sql.append("users.name as name, ");
            sql.append("users.created_date as created_date, ");
            sql.append("branch.branchName as branchName, ");
            sql.append("department.departmentName as departmentName ");
            sql.append("FROM users ");
            sql.append("INNER JOIN branch ");
            sql.append("ON users.branchId = branch.id ");
            sql.append("INNER JOIN department ");
            sql.append("ON users.departmentId = department.id ");
            sql.append("ORDER BY created_date DESC limit " + num);

            ps = connection.prepareStatement(sql.toString());

            ResultSet rs = ps.executeQuery();
            List<UserBranchDepartment> ret = toUserBranchDepartmentList(rs);
            return ret;
        } catch (SQLException e) {
            throw new SQLRuntimeException(e);
        } finally {
            close(ps);
        }
    }

    private List<UserBranchDepartment> toUserBranchDepartmentList(ResultSet rs)
            throws SQLException {

        List<UserBranchDepartment> ret = new ArrayList<UserBranchDepartment>();
        try {
            while (rs.next()) {
                String login_id = rs.getString("login_id");
                String name = rs.getString("name");
                int stopId = rs.getInt("stopId");
                int id = rs.getInt("id");
                int branchId = rs.getInt("branchId");
                int departmentId = rs.getInt("departmentId");
                String branchName = rs.getString("branchName");
                String departmentName = rs.getString("departmentName");
                Timestamp createdDate = rs.getTimestamp("created_date");

                UserBranchDepartment user = new UserBranchDepartment();
                user.setLogin_id(login_id);
                user.setStopId(stopId);
                user.setName(name);
                user.setId(id);
                user.setBranchId(branchId);
                user.setDepartmentId(departmentId);
                user.setBranchName(branchName);
                user.setDepartmentName(departmentName);
                user.setCreatedDate(createdDate);

                ret.add(user);
            }
            return ret;
        } finally {
            close(rs);
        }
    }

}