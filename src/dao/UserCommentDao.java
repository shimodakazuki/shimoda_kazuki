package dao;

import static utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import beans.UserComment;
import exception.SQLRuntimeException;

public class UserCommentDao {

    public List<UserComment> getUserComments(Connection connection, int num) {

        PreparedStatement ps = null;
        try {
            StringBuilder sql = new StringBuilder();
            sql.append("SELECT ");
            sql.append("comments.id as id, ");
            sql.append("comments.post as post, ");
            sql.append("comments.userId as userId, ");
            sql.append("comments.postId as postId, ");
            sql.append("comments.deleteId as deleteId, ");
            sql.append("users.name as name, ");
            sql.append("comments.created_date as created_date ");
            sql.append("FROM comments ");
            sql.append("INNER JOIN users ");
            sql.append("ON comments.userId = users.id ");
            sql.append("ORDER BY created_date DESC limit " + num);

            ps = connection.prepareStatement(sql.toString());

            ResultSet rs = ps.executeQuery();
            List<UserComment> ret = toUserCommentList(rs);
            return ret;
        } catch (SQLException e) {
            throw new SQLRuntimeException(e);
        } finally {
            close(ps);
        }
    }

    private List<UserComment> toUserCommentList(ResultSet rs)
            throws SQLException {

        List<UserComment> ret = new ArrayList<UserComment>();
        try {
            while (rs.next()) {
                String post = rs.getString("post");
                int id = rs.getInt("id");
                int userId = rs.getInt("userId");
                int postId = rs.getInt("postId");
                int deleteId = rs.getInt("deleteId");
                Timestamp createdDate = rs.getTimestamp("created_date");
                String name = rs.getString("name");

                UserComment comment = new UserComment();
                comment.setPost(post);
                comment.setUserId(userId);
                comment.setPostId(postId);
                comment.setId(id);
                comment.setDeleteId(deleteId);
                comment.setCreated_date(createdDate);
                comment.setName(name);

                ret.add(comment);
            }
            return ret;
        } finally {
            close(rs);
        }
    }

}