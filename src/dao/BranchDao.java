package dao;

import static utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import beans.Branch;
import exception.SQLRuntimeException;

public class BranchDao {

    public void insert(Connection connection, Branch branch) {

        PreparedStatement ps = null;
        try {
            StringBuilder sql = new StringBuilder();
            sql.append("INSERT INTO branch ( ");
            sql.append("branchName");
            sql.append(") VALUES (");
            sql.append(" ?"); //branchName
            sql.append(")");

            ps = connection.prepareStatement(sql.toString());

            ps.setString(1, branch.getBranchName());

            ps.executeUpdate();
        } catch (SQLException e) {
            throw new SQLRuntimeException(e);
        } finally {
            close(ps);
        }
    }

}