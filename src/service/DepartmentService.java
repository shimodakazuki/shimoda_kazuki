package service;

import static utils.CloseableUtil.*;
import static utils.DBUtil.*;

import java.sql.Connection;

import beans.Department;
import dao.DepartmentDao;


public class DepartmentService {

    public void register(Department department) {

        Connection connection = null;
        try {
            connection = getConnection();

            DepartmentDao departmentDao = new DepartmentDao();
            departmentDao.insert(connection, department);

            commit(connection);
        } catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
        }
    }
}