package filter;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.User;
import service.UserService;

@WebFilter("/*")
public class RestrictionFilter implements Filter {

	@Override
	public void doFilter(ServletRequest request, ServletResponse response,
			FilterChain chain) throws IOException, ServletException {

		if(((HttpServletRequest)request).getServletPath().equals("/userManagement") ||
				((HttpServletRequest)request).getServletPath().equals("/settings") ||
				((HttpServletRequest)request).getServletPath().equals("/signup")) {

			HttpSession session = ((HttpServletRequest)request).getSession();
			User loginUser = (User) session.getAttribute("loginUser");
			User editUser = new UserService().getUser(loginUser.getId());

			if(editUser.getDepartmentId() != 1){
				List<String> messages = new ArrayList<String>();
				messages.add("権限がありません。");
				session.setAttribute("errorMessages", messages);
				((HttpServletResponse)response).sendRedirect("top");
				return;
			}
		}
		chain.doFilter(request, response); // サーブレットを実行
	}
	@Override
	public void init(FilterConfig config) {
	}
	@Override
	public void destroy() {
	}
}
