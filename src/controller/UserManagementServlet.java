package controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.User;
import beans.UserBranchDepartment;
import service.UserService;

@WebServlet(urlPatterns = { "/userManagement" })
public class UserManagementServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws IOException, ServletException {

		List<UserBranchDepartment> users = new UserService().getUser();

		request.setAttribute("users", users);

		request.getRequestDispatcher("user_management.jsp").forward(request, response);
	}

	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws IOException, ServletException {

		HttpSession session = request.getSession();
		User editUser =  new User();

		editUser.setStopId(Integer.parseInt(request.getParameter("stopId")));
		editUser.setId(Integer.parseInt(request.getParameter("id")));

		session.setAttribute("stopUser", editUser);

		new UserService().stopUser(editUser);

		response.sendRedirect("userManagement");
	}
}

