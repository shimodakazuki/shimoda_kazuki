package controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;

import beans.User;
import exception.NoRowsUpdatedRuntimeException;
import service.UserService;

@WebServlet(urlPatterns = { "/settings" })
public class SettingsServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {


		User editUser = new UserService().getUser(Integer.parseInt(request.getParameter("id")));
		request.setAttribute("editUser", editUser);

		request.getRequestDispatcher("settings.jsp").forward(request, response);
	}

	@Override
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {

		List<String> messages = new ArrayList<String>();
		HttpSession session = request.getSession();
		User editUser = getEditUser(request);

		if (isValid(request, messages) == true) {

			try {
				new UserService().update(editUser);
			} catch (NoRowsUpdatedRuntimeException e) {
				messages.add("他の人によって更新されています。最新のデータを表示しました。データを確認してください。");
				session.setAttribute("errorMessages", messages);
				request.setAttribute("editUser", editUser);
				request.getRequestDispatcher("settings.jsp").forward(request, response);
				return;
			}

			session.setAttribute("loginUser", editUser);

			response.sendRedirect("top");
		} else {
			session.setAttribute("errorMessages", messages);
			request.setAttribute("editUser", editUser);
			request.getRequestDispatcher("settings.jsp").forward(request, response);
		}
	}

	private User getEditUser(HttpServletRequest request)
			throws IOException, ServletException {

		User editUser = new User();

		editUser.setId(Integer.parseInt(request.getParameter("id")));
		editUser.setName(request.getParameter("name"));
		editUser.setLogin_id(request.getParameter("login_id"));
		editUser.setPassword(request.getParameter("password"));
		editUser.setBranchId(Integer.parseInt(request.getParameter("branchId")));
		editUser.setDepartmentId(Integer.parseInt(request.getParameter("departmentId")));

		return editUser;
	}


	private boolean isValid(HttpServletRequest request, List<String> messages) {

		String name = request.getParameter("name");
		String login_id = request.getParameter("login_id");
		String password = request.getParameter("password");
		String confirmationPassword = request.getParameter("confirmationPassword");

		if (StringUtils.isEmpty(name) == true) {
			messages.add("名称を入力してください");
		}
		if (StringUtils.isEmpty(login_id) == true) {
			messages.add("ログインIDを入力してください");
		}
		if (StringUtils.isEmpty(name) == false && 10 < name.length()) {
			messages.add("名称は10文字以下で入力してください");
		}
		if (StringUtils.isEmpty(login_id) == false && 6 > login_id.length()) {
			messages.add("ログインIDは6文字以上で入力してください");
		}
		if (StringUtils.isEmpty(login_id) == false && 20 < login_id.length()) {
			messages.add("ログインIDは20文字以下で入力してください");
		}
		if (StringUtils.isEmpty(login_id) == false && !(login_id.matches("[-_@+*;:#$%&A-Za-z0-9]+"))) {
			messages.add("ログインIDは半角英数字で入力してください");
		}
		if (StringUtils.isEmpty(password) == false && 6 > password.length()) {
			messages.add("パスワードは6文字以上で入力してください");
		}
		if (StringUtils.isEmpty(password) == false && 20 < password.length()) {
			messages.add("パスワードは20文字以下で入力してください");
		}
		if (StringUtils.isEmpty(password) == false &&!(password.matches("[-_@+*;:#$%&A-Za-z0-9]+"))) {
			messages.add("パスワードは半角英数字で入力してください");
		}
		if (!(password.equals(confirmationPassword))) {
			messages.add("パスワードとパスワード(確認用)が一致しません");
		}
		// TODO アカウントが既に利用されていないか、メールアドレスが既に登録されていないかなどの確認も必要
		if (messages.size() == 0) {
			return true;
		} else {
			return false;
		}
	}
}