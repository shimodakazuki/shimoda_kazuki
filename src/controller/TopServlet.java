package controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import beans.User;
import beans.UserComment;
import beans.UserMessage;
import service.CommentService;
import service.MessageService;


@WebServlet(urlPatterns = { "/top" })
public class TopServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws IOException, ServletException {

		User user = (User) request.getSession().getAttribute("loginUser");
		boolean isShowMessageForm;
		if (user != null) {
			isShowMessageForm = true;
		} else {
			isShowMessageForm = false;
		}

		String keyword = request.getParameter("keyword");
		String startDate = request.getParameter("startDate");
		String lastDate = request.getParameter("lastDate");

		if(keyword != null) {
		List<UserMessage> userMessage = new MessageService().keyword(keyword);
		request.setAttribute("messages", userMessage);
		}

		if(startDate != null && lastDate != null) {
			List<UserMessage> dateMessage = new MessageService().date(startDate , lastDate);
			System.out.println(dateMessage.size());
			request.setAttribute("messages", dateMessage);
		}

		if(keyword == null && startDate == null && lastDate == null) {
		List<UserMessage> messages = new MessageService().getMessage();
		request.setAttribute("messages", messages);
		}

		request.setAttribute("isShowMessageForm", isShowMessageForm);

		List<UserComment> comments = new CommentService().getComment();
		request.setAttribute("comments", comments);

		request.getRequestDispatcher("/top.jsp").forward(request, response);
	}
}